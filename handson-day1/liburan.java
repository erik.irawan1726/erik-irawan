import java.util.Scanner;
public class liburan {
    public static void main(String[] args) {
        Scanner sca = new Scanner(System.in);

        System.out.println("Selamat di Jalan-Jajan Travel");
        System.out.println("Paket yang tersedia yaitu :");
        System.out.println("    Bandung");
        System.out.println("    Jepang");
        System.out.println("    USA");
        System.out.print("Silahkan pilih daerah tujuan anda :");
        String kota = sca.nextLine();
        switch (kota) {
            case "Bandung":
                System.out.println("Destinasi Wisata :");
                System.out.println("    -Tangkuban Perahu");
                System.out.println("    -Jalan Braga");
                System.out.println("    -Gedung Sate");
                break;
            case "Jepang":
                System.out.println("Destinasi Wisata :");
                System.out.println("    -Tokyo skytree");
                System.out.println("    -Meiji Jingu");
                System.out.println("    -Shinjuku Gyeon National Garden");
                break;
            case "USA":
                System.out.println("Destinasi Wisata :");
                System.out.println("    -Central Park");
                System.out.println("    -The Metropolitan Museum of Art");
                System.out.println("    -Empire State Building");
                break;
        }
        System.out.println("Apakah anda ingin melihat detail biaya ? ketik y/n");
        String konf = sca.nextLine();
        switch (konf) {
            case "y" :
                if(kota.equals("Bandung")) {
                    System.out.println("Biaya perjalanan sejumlah 2jt/org");
                } else if(kota.equals("Jepang")) {
                    System.out.println("Biaya perjalanan sejumlah 16jt/org");
                } else if(kota.equals("USA")) {
                    System.out.println("Biaya perjalanan sejumlah 37.5jt/org");
                } else {
                    System.out.println("Silahkan pilih destinasi perjalanan anda terlebih dahulu");
                }
                break;
            case "n" :
                System.out.println("Terimakasih telah menggunakan Aplikasi kami");
                break;
        }
    }
}
